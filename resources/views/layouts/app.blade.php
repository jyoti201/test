<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="https://wiseint.grandcentr.al/images/favicon.ico" type="image/png">
    <title>GrandCentral | Your Web Dashboard</title>
	<link href="https://wiseint.grandcentr.al/css/app.css" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <!-- page dependent styles -->
	<script>var slider_speed_default = 0</script>
	<script src="https://wiseint.grandcentr.al/js/script.js"></script>
	
	@yield('extra')
</head>
<body class="left-navigation pace-done">
<div id="wrapper">
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<li class=" active">
				<a href="https://wiseint.grandcentr.al/website/setting/1">
				<i class="fa fa-thumb-tack"></i> <span class="nav-label"> Pages</span>
				</a>
			</li>
		</ul>
	</div>
</nav>            
<div id="page-wrapper" class="gray-bg" style="min-height: 900px;">
	<div class="main_container">
		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="row">
				<div class="col-lg-12">
					<h2>
					Slider
					</h2>
				</div>
			</div>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight revslide">
				@yield('content')
		</div>
	</div>
</div>

	@yield('footer')
</body>
</html>
