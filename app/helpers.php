<?php
function getSliderSetting($key,$value){
	$defaults = array(
		'gridwidth'=>  '1240',
		'gridheight'=> '600',
		'sliderType'=> 'standard',
		'sliderLayout'=> 'auto',
		'delay'=> '9000',
		'shadow'=> '0',
		'spinner'=> 'off',
		'stoploop'=> 'off',
		'stopafterloop'=> '-1',
		'stopatslide'=> '-1',
		'autoheight'=> 'off',
		'hidethumbsonmobile'=> 'off',
		'enablearrow'=> 'true',
		'arrow_style'=> 'gyges',
		'hideonmobile'=> 'true',
		'hideunder'=> '600',
		'hideonleave'=> 'true',
		'l_halign'=> 'left',
		'l_valign'=> 'center',
		'l_horizoff'=> '30',
		'l_vertoff'=> '0',
		'r_halign'=> 'right',
		'r_valign'=> 'center',
		'r_horizoff'=> '30',
		'r_vertoff'=> '0',
		'bgimage' => 'http://test.swenspace.com/uploads/thomas-jefferson-1.jpg'
	);
	if(empty($value)){
		return $defaults[$key];
	}else{
		return $value;
	}
}
function getOptions($key){
	$defaultOptions = array(
		'sliderType' => array('standard'=>'Standard','hero'=>'Hero','carousel'=>'Carousel'),
		'sliderLayout' => array('auto'=>'Auto','fullwidth'=>'Fullwidth','fullscreen'=>'Fullscreen'),
		'shadow'=> array('0'=>'No Shadow','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6'),
		'spinner'=> array('off','0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'),
		'arrow_style'=> array('round'=>'Hesperiden','navbar'=>'Gyges','preview1'=>'Hades','preview2'=>'Ares','preview3'=>'Hebe','preview4'=>'Hermes','custom'=>'Custom','round-old'=>'Hephaistos','square-old'=>'Persephone','navbar-old'=>'Erinyen','zeus'=>'Zeus','metis'=>'Metis','dione'=>'Dione','uranus'=>'Uranus'),
		'l_halign'=> array('left'=>'Left','center'=>'Center','right'=>'Right'),
		'l_valign'=> array('top'=>'Top','center'=>'Center','bottom'=>'Bottom'),
		'r_halign'=> array('left'=>'Left','center'=>'Center','right'=>'Right'),
		'r_valign'=> array('top'=>'Top','center'=>'Center','bottom'=>'Bottom'),
		'fontweight' => array('100'=>'100','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700','800'=>'800','900'=>'900'),
		'halign' => array('left'=>'Left','right'=>'Right','center'=>'Center'),
		'valign' => array('left'=>'Left','right'=>'Right','center'=>'Center'),
		'slideanimation' => array(
			'Basics' => array('notransition'=>'No Transition', 'fade'=>'Fade','crossfade'=>'Fade Cross','fadethroughdark'=>'Fade Through Black','fadethroughlight'=>'Fade Through Light','fadethroughtransparent'=>'Fade Through Transparent'),
			'Slide Simple' => array('slideup'=> 'Slide To Top', 'slidedown'=>'Slide To Bottom','slideright'=>'Slide To Right','slideleft'=>'Slide To Left','slidehorizontal'=>'Slide Horizontal (Next/Previous)','slidevertical'=>'Slide Vertical (Next/Previous)'),
			'Slide Over'  => array('slideoverup'=>'Slide Over To Top','slideoverdown'=>'Slide Over To Bottom','slideoverright'=>'Slide Over To Right','slideoverleft'=>'Slide Over To Left','slideoverhorizontal'=>'Slide Over Horizontal (Next/Previous)','slideoververtical'=>'Slide Over Vertical (Next/Previous)'),
			'Slide Remove' => array('slideremoveup'=>'Slide Remove To Top','slideremovedown'=>'Slide Remove To Bottom','slideremoveright'=>'Slide Remove To Right','slideremoveleft'=>'Slide Remove To Left','slideremovehorizontal'=>'Slide Remove Horizontal (Next/Previous)','slideremovevertical'=>'Slide Remove Vertical (Next/Previous)'),
			'Sliding Overlays' => array('slidingoverlayup'=>'Sliding Overlays To Top','slidingoverlaydown'=>'Sliding Overlays To Bottom','slidingoverlayright'=>'Sliding Overlays To Right','slidingoverlayleft'=>'Sliding Overlays To Left','slidingoverlayhorizontal'=>'Sliding Overlays Horizontal (Next/Previous)','slidingoverlayvertical'=>'Sliding Overlays Vertical (Next/Previous)'),
			'Slots and Boxes' => array('boxslide'=>'Slide Boxes','slotslide-horizontal'=>'Slide Slots Horizontal','slotslide-vertical'=>'Slide Slots Vertical','boxfade'=>'Fade Boxes','slotfade-horizontal'=>'Fade Slots Horizontal','slotfade-vertical'=>'Fade Slots Vertical'),
			'Fade and Slide' => array('fadefromright'=>'Fade and Slide from Right','fadefromleft'=>'Fade and Slide from Left','fadefromtop'=>'Fade and Slide from Top','fadefrombottom'=>'Fade and Slide from Bottom','fadetoleftfadefromright'=>'To Left From Right','fadetorightfadefromleft'=>'To Right From Left','fadetotopfadefrombottom'=>'To Top From Bottom','fadetobottomfadefromtop'=>'To Bottom From Top'),
			'Parallax' => array('parallaxtoright'=>'Parallax to Right','parallaxtoleft'=>'Parallax to Left','parallaxtotop'=>'Parallax to Top','parallaxtobottom'=>'Parallax to Bottom','parallaxhorizontal'=>'Parallax Horizontal','parallaxvertical'=>'Parallax Vertical'),
			'Zoom Transitions' => array('scaledownfromright'=>'Zoom Out and Fade From Right','scaledownfromleft'=>'Zoom Out and Fade From Left','scaledownfromtop'=>'Zoom Out and Fade From Top','scaledownfrombottom'=>'Zoom Out and Fade From Bottom','zoomout'=>'ZoomOut','zoomin'=>'ZoomIn','slotzoom-horizontal'=>'Zoom Slots Horizontal','slotzoom-vertical'=>'Zoom Slots Vertical'),
			'Curtain Transitions' => array('curtain-1'=>'Curtain from Left','curtain-2'=>'Curtain from Right','curtain-3'=>'Curtain from Middle'),
			'Filter Transitions' => array('grayscale'=>'Grayscale Transition','grayscalecross'=>'Grayscale Cross Transition','brightness'=>'Brightness Transition','brightnesscross'=>'Brightness Cross Transition','blurlight'=>'Blur Light Transition','blurlightcross'=>'Blur Light Cross Transition','blurstrong'=>'Blur Strong Transition','blurstrongcross'=>'Blur Strong Cross Transition'),
			'Premium Transitions' => array('3dcurtain-horizontal'=>'3D Curtain Horizontal','3dcurtain-vertical'=>'3D Curtain Vertical','cube'=>'Cube Vertical','cube-horizontal'=>'Cube Horizontal','incube'=>'In Cube Vertical','incube-horizontal'=>'In Cube Horizontal','turnoff'=>'TurnOff Horizontal','turnoff-vertical'=>'TurnOff Vertical','papercut'=>'Paper Cut','flyin'=>'Fly In'),
			'Random' => array('random-selected'=>'Random of Selected','random-static'=>'Random Flat','random-premium'=>'Random Premium','random'=>'Random Flat and Premium')
		),
		'layeranimation' => array(
			'noanimation' => array('label'=>'No Animation', 'param'=>'[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'fadein' => array('label'=>'Fade In', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'shortfromtop' => array('label'=>'Short From Top', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'shortfrombottom' => array('label'=>'Short From Bottom', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'),
			'shortfromleft' => array('label'=>'Short From Left', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'shortfromright' => array('label'=>'Short From Right', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"x:50px;opacity:0;","ease":"Power3.easeInOut"}]'),
			'longfromright' => array('label'=>'Long From Right', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'longfromleft' => array('label'=>'Long From Left', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'longfromtop' => array('label'=>'Long From Top', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'longfrombottom' => array('label'=>'Long From Bottom', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"y:bottom;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'skewfromlongleft' => array('label'=>'Skew From Long Left', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:left;skX:45px;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'skewfromlongright' => array('label'=>'Skew From Long Right', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:right;skX:-85px;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'skewfromshortleft' => array('label'=>'Skew From Short Left', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:-200px;skX:85px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'skewfromshortright' => array('label'=>'Skew From Short Right', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:200px;skX:-85px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'randomrotateandscale' => array('label'=>'Random Rotate And Scale', 'param'=>'[{"delay":0,"speed":300,"frame":"0","from":"x:{-250,250};y:{-150,150};rX:{-90,90};rY:{-90,90};rZ:{-360,360};sX:0;sY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'letterflyinfrombottom' => array('label'=>'Letter Fly In From Bottom', 'param'=>'[{"delay":0,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[100%];z:0;rZ:-35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'lettersflyinfromleft' => array('label'=>'Letters Fly In From Left', 'param'=>'[{"delay":0,"split":"chars","splitdelay":0.1,"speed":2000,"frame":"0","from":"x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'lettersflyinfromright' => array('label'=>'Letters Fly In From Right', 'param'=>'[{"delay":0,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'lettersflyinfromtop' => array('label'=>'Letters Fly In From Top', 'param'=>'[{"delay":0,"split":"chars","splitdelay":0.05,"speed":2000,"frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'maskedzoomout' => array('label'=>'Masked Zoom Out', 'param'=>'[{"delay":0,"speed":1000,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'popupsmooth' => array('label'=>'Pop Up Smooth', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'rotateinfrombottom' => array('label'=>'Rotate In From Bottom', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"y:bottom;rZ:90deg;sX:2;sY:2;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'rotateinfromzero' => array('label'=>'Rotate In From Zero', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'slidemaskfrombottom' => array('label'=>'Slide Mask From Bottom', 'param'=>'[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'slidemaskfromleft' => array('label'=>'Slide Mask From Left', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'slidemaskfromright' => array('label'=>'Slide Mask From Right', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'slidemaskfromtop' => array('label'=>'Slide Mask From Top', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothpopupone' => array('label'=>'Smooth Popup One', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothpopuptwo' => array('label'=>'Smooth Popup Two', 'param'=>'[{"delay":0,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothmaskfromright' => array('label'=>'Smooth Mask From Right', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothmaskfromleft' => array('label'=>'Smooth Mask From Left', 'param'=>'[{"delay":0,"speed":1500,"frame":"0","from":"x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'),
			'smoothslidefrombottom' => array('label'=>'Smooth Slide From Bottom', 'param'=>'[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]')
		)
		// https://www.themepunch.com/revsliderjquery-doc/layer-transitions/
	);
	return $defaultOptions[$key];
}
?>