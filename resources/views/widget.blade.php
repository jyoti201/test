@extends('layouts.app')

@section('extra')

<link rel="stylesheet" href="{{ asset('widget-assets/css/squares.css') }}">
<link rel="stylesheet" href="{{ asset('widget-assets/css/squares-editor.css') }}">
<link rel="stylesheet" href="{{ asset('widget-assets/css/squares-controls.css') }}">
<link rel="stylesheet" href="{{ asset('widget-assets/css/wcp-editor.css') }}">
<link rel="stylesheet" href="{{ asset('widget-assets/css/wcp-editor-controls.css') }}">
<link rel="stylesheet" href="{{ asset('widget-assets/css/image-map-pro-editor.css') }}">
<link rel="stylesheet" href="{{ asset('widget-assets/css/image-map-pro.css') }}">
<style>
#wcp-editor-button-new,#wcp-editor-button-save,#wcp-editor-button-load,#wcp-editor-extra-main-buttons{
	display:none !important;
}
#wcp-editor-left {
    width: 320px;
}
#wcp-editor-right {
    height: auto;
}
#wcp-editor {
    min-height: 700px;
}
#wcp-editor-textarea-export{
	
}
</style>
<script>
$(document).ready(function(){
	
	/* Preload Saved Map */
	setTimeout(function(){ 
		$('div[data-wcp-form-tab-button-name="image"]').trigger('click');
		$('#wcp-editor-extra-main-buttons .wcp-editor-extra-main-button').each(function(){
			if($(this).attr('data-wcp-editor-extra-main-button-name')=='import'){
				
				$(this).trigger('click');
				$('#wcp-editor-modal').hide();
				
				/* Load Save Data */
				$('#wcp-editor-textarea-import').val('{"id":811,"editor":{"previewMode":1,"selected_shape":"rect-1857","tool":"poly"},"general":{"name":"Demo","width":960,"height":582,"naturalWidth":960,"naturalHeight":582,"preserve_quality":0},"image":{"url":"https://webcraftplugins.com/uploads/image-map-pro/demo_low.png"},"tooltips":{"constrain_tooltips":0,"fullscreen_tooltips":"mobile-only"},"spots":[{"id":"rect-2487","title":"rect-2487","type":"poly","x":47.334,"y":28.284,"width":0,"height":0,"tooltip_content":{"squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":null,"y":null}]},{"id":"rect-1857","title":"rect-1857","type":"poly","x":12.278,"y":29.084,"width":20.194,"height":39.172,"actions":{"click":"follow-link","link":"http:ggshdsd.com"},"tooltip_content":{"squares_settings":{"containers":[{"id":"sq-container-403761","settings":{"elements":[{"settings":{"name":"Paragraph","iconClass":"fa fa-paragraph"}}]}}]}},"points":[{"x":82.39999999999999,"y":7.482993197278912},{"x":100,"y":61.9047619047619},{"x":87.2,"y":98.63945578231292},{"x":20.8,"y":100},{"x":0,"y":36.734693877551024},{"x":0,"y":0}]}]}')
				/* Load Save Data */
				
				
				
				
				
				$('#wcp-editor-confirm-import').trigger('click');
			}
		})
	}, 500);

	$('#save').on('click', function(){
		$('#wcp-editor-extra-main-buttons .wcp-editor-extra-main-button').each(function(){
			if($(this).attr('data-wcp-editor-extra-main-button-name')=='export'){
				$(this).trigger('click');
				$('#wcp-editor-modal').hide();
				
				
				
				
				
				/* Save Data */
				alert($('#wcp-editor-textarea-export').val());
				/* Save Data */
				
				
				
				
				
				
			}
		})
	})
})
</script>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content text-right">
				<a href="#" id="save" class="btn btn-primary">Save</a>
			</div>
			<div class="ibox-content" style="padding:0;">
				<div id="wcp-editor">
        
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer')

<script src="{{ asset('widget-assets/js/squares-renderer.js') }}"></script>
<script src="{{ asset('widget-assets/js/squares.js') }}"></script>
<script src="{{ asset('widget-assets/js/squares-elements-jquery.js') }}"></script>
<script src="{{ asset('widget-assets/js/squares-controls.js') }}"></script>
<script src="{{ asset('widget-assets/js/wcp-editor.js') }}"></script>
<script src="{{ asset('widget-assets/js/wcp-editor-controls.js') }}"></script>
<script src="{{ asset('widget-assets/js/wcp-compress.js') }}"></script>
<script src="{{ asset('widget-assets/js/wcp-icons.js') }}"></script>
<script src="{{ asset('widget-assets/js/image-map-pro-defaults.js') }}"></script>
<script src="{{ asset('widget-assets/js/image-map-pro-editor.js') }}"></script>
<script src="{{ asset('widget-assets/js/image-map-pro-editor-content.js') }}"></script>
<script src="{{ asset('widget-assets/js/image-map-pro-editor-local-storage.js') }}"></script>
<script src="{{ asset('widget-assets/js/image-map-pro-editor-init-jquery.js') }}"></script>
<script>
var t = jQuery;
var o = document;
t(o).ready(function() {
	var e= {
		id:811, editor: {
			previewMode: 0, selected_shape: "poly-974", tool: "select"
		}
		, general: {
			name: "Demo", width: 909, height: 1000, naturalWidth: 909, naturalHeight: 1000, preserve_quality: 0
		}
		, image: {
			url: "http://test.swenspace.com/uploads/mapsample.png" 
		}
		, tooltips: {
			constrain_tooltips: 0, fullscreen_tooltips: "mobile-only"
		}
		, spots:[ ]
	}
	;
	t.image_map_pro_init_editor(e, t.WCPEditorSettings)
}
)
</script>
<script src="{{ asset('widget-assets/js/image-map-pro.js') }}"></script>

@endsection
