@extends('layouts.app')

@section('extra')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="{{ asset('revolution/css/jquery.ui.ruler.css') }}">
<script src="{{ asset('revolution/js/jquery.ui.ruler.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('revolution/fonts/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/settings.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/layers.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/navigation.css') }}">
<script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
<script language="JavaScript">
	$(document).ready(function () {
		var hash = location.hash.substr(1);
		if(hash == 'setting'){
			$('#'+hash).trigger('click');
		}
		$('#editor').ruler();
		$('.ruler.top').click(function(e){
			var pos = parseFloat(e.pageX) - parseFloat($('#editor').offset().left);
			$('#editor').append("<span class='line' style='top:0;bottom:0;width:1px;background:#000000;left:"+pos+"px'></span>");
		})
		$('.ruler.left').click(function(e){
			var pos = parseFloat(e.pageY) - parseFloat($('#editor').offset().top);
			$('#editor').append("<span class='line' style='left:0;right:0;height:1px;background:#000000;top:"+pos+"px'></span>");
		})
		$(function(){
			
			
			$( "#editor").on('click','.tp-mask-wrap',function(){
				$( ".tp-mask-wrap").removeClass('selected');
				$("#frames li").removeClass('selected');
				jQuery(this).addClass('selected');
				$("#frames li[data-rel='"+jQuery(this).find('.tp-caption').attr('id')+"']").addClass('selected');
				
				$( ".tp-mask-wrap.selected" ).draggable({
						stop: function getNewPos(e, ui) {
							e.stopPropagation(); 
							$('#styles input[name="layer_hoffset"]').val(ui.position.left);
							$('#frames li.selected input[name="hidden_layer_hoffset[]"]').val(ui.position.left);
							$( ".tp-mask-wrap.selected .tp-caption").attr('data-hoffset', ui.position.left);
							$('#frames li.selected input[name="hidden_layer_voffset[]"]').val(ui.position.top);
							$('#styles input[name="layer_voffset"]').val(ui.position.top);
							$( ".tp-mask-wrap.selected .tp-caption").attr('data-voffset', ui.position.top);
						}
					});
				$('#textlayer').val($('#frames li.selected input[name="hidden_layer_text[]"]').val());
				$('#styles .form-control').each(function(){
					if($(this).prop('type')=='text'){
						$(this).val('');
						$(this).val($('#frames li.selected input[name="hidden_'+$(this).attr('name')+'[]"]').val());
					}else if($(this).prop('type')=='select-one'){
						$(this).find("option").each(function(){
							$(this).removeAttr('selected');
						});
						var val = $('#frames li.selected input[name="hidden_'+$(this).attr('name')+'[]"]').val();
						$(this).find("option[value='"+val+"']").prop("selected", true);
					}
				})
			})
			
			$(document).on("click", function(e) {
				if ($(e.target).is("#textlayer") === false) {
				  $("#textlayer").hide();
				  $('#savelayer').prop("disabled", true);
				  $('#editlayer').prop("disabled", false);
				}
			 });
			$('.styles select.form-control').change(function(){
				$('#frames li.selected input[name="hidden_'+$(this).attr('name')+'[]"]').val($(this).val());
			})
			$('.styles input.form-control').bind('change paste keyup',function(){
				$('#frames li.selected input[name="hidden_'+$(this).attr('name')+'[]"]').val($(this).val());
			})
		});
	});
</script>
<style>
.revslide .slidethumbs{
	list-style: none;
    margin: 0;
    padding: 0;
}
.revslide .slidethumbs li{
	display: inline-block;
    width: 180px;
	height:130px;
	background-size:cover;
	background-position:center;
	margin:10px;
	position:relative;
	    vertical-align: top;
}
.revslide .slidethumbs li .slidecount{
	background: #000000;
    color: #ffffff;
    text-align: center;
    padding: 5px;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
}
.revslide .slidethumbs li img{
	max-width:100%;
}
.revslide .nav>li.active{
	border:0;
}
.revslide .editor{
	position:relative;
	padding:0;
}
.revslide #editor{
	position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    padding: 30px;
}
.revslide #editor .line{
	position:absolute;
}
.revslide .ruler{
	cursor:pointer;
}
.revslide #editor .ef-ruler .stage {
    width: 100%;
    height: 100%;
}
.revslide .editor{
	height:{{ getSliderSetting('gridheight', '')+25 }}px;
	    padding-top: 45px;
		overflow:auto;
}
.revslide #editor{
	width: {{ getSliderSetting('gridwidth', '') }}px;margin-top: 45px;
	    overflow: hidden;
}
.revslide .buttons{
	list-style:none;
	text-align:right;
	margin:0;
}
.revslide .buttons li{
	display:inline-block;
	margin-left: -3px;
}
.revslide .buttons li button{
	font-size: 23px;
    color: #ffffff;
    display: block;
    width: 50px;
    text-align: center;
    height: 50px;
    line-height: 57px;
    background: transparent;
    border-style: none;
    border-left: 1px solid #000000;
}
.revslide .buttons li button:hover{
	background:#319cf4;
}
.revslide #settings .tab-content{
	padding-top:20px;
}
.revslide #settings h4{
	font-weight:600;
}
.revslide #settings .form-control-short{
	display:inline-block;
	max-width:100px;
}
.revslide .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.revslide .switch input {display:none;}

.revslide .slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.revslide .slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

.revslide input:checked + .slider {
  background-color: #2196F3;
}

.revslide input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

.revslide input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.revslide .slider.round {
  border-radius: 34px;
}

.revslide .slider.round:before {
  border-radius: 50%;
}
.revslide .tp-mask-wrap:hover{
	cursor:move;
	border:2px solid #3097D1;
	margin-top:-2px;
	margin-left:-2px;
}
.revslide .tp-mask-wrap.selected{
	margin-top:-2px;
	margin-left:-2px;
	border:2px solid #3097D1;
}
.revslide .nav-pills > li > a {
    border-radius: 0px;
}
.revslide .slidethumbs li.new{
	border-left: 1px dashed #cccccc;
    border-top: 1px dashed #cccccc;
    border-right: 1px dashed;
}
.revslide .slidethumbs li.new a{
	font-size: 35px;
    color: #000000;
    text-align: center;
    line-height: 3.2;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}
.revslide .inline-field{
	display: inline-block;
    margin: 5px 10px;
}
.revslide .inline-field label{
	margin-right: 6px;
}
.revslide .inline-field input{
	width: 70px;
	background: #cccccc;
    border-style: none;
    border-radius: 5px;
	color:#000000;
	padding: 0 10px;
    font-family: arial;
}
.revslide .inline-field select{
	width: 70px;  
	padding: 4px;
    background: #cccccc;
    border-radius: 5px;
    border: 0;
}
.revslide .inline-field .short{
	margin-right:2px;
}
.revslide .inline-field .form-control{
	display:inline-block;
}
.revslide .layer_controls{
	position: absolute;
    top: 0px;
    left: 0;
    right: 0;
	background: #000000;
    color: #ffffff;
	padding-top: 5px;
}
.revslide .ef-ruler{
	border-top: 1px solid #cccccc;
	overflow: visible;
}
.revslide .layer_controls .btn{
	background: #ffffff;
    color: #000000;
    font-weight: 600;
}
.revslide .textlayer{
	position: absolute;
    left: 50%;
    top: 100%;
    z-index: 999;
    width: 100%;
    max-width: 300px;
    height: 95px;
    transform: translateX(-50%);
	    padding: 10px;
	display:none;
	background:#000000;
}
.revslide .radio-enabled .form-control{
	display:none;
	max-width:400px;
	margin-left:10px;
}
.revslide .radio-enabled input[type=radio]:checked + .form-control{
	display:inline-block;
}
.revslide .radio-enabled h4{
	display:inline-block;
	width: 240px;
}
.revslide .layer_controls ul{
	list-style:none;
	    margin-bottom: 0;
}
.revslide .layer_controls ul li{
	display:inline-block;
}
.revslide .layer_controls ul li ul{
	position: absolute;
    z-index: 999;
    background: #000000;
    padding: 0;
	border-bottom-left-radius:3px;
	border-bottom-right-radius:3px;
	display:none;
}
.revslide .layer_controls ul li:hover ul{
	display:block;
}
.revslide .layer_controls ul li ul li{
	display:block;
}
.revslide .layer_controls ul li ul li a{
	display: block;
    padding: 5px 40px;
    color: #999999;
    font-weight: bold;
	text-decoration:none;
}
.revslide .layer_controls ul li ul li a:hover{
	color:#ffffff;
}








.revslide .vertical-tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 20%;
}

/* Style the buttons that are used to open the tab content */
.revslide .vertical-tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 10px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
.revslide .vertical-tab button:hover {
    background-color: #ddd;
}

/* Create an active/current "tab button" class */
.revslide .vertical-tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.revslide .tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 35%;
    border-left: none;
	border-right:none;
	display:none;
}
.revslide .tabcontent.active{
	display:block;
}
.revslide .tabcontent ul{
	    list-style: none;
    padding: 20px 10px;
    margin: 0;
}
.revslide .selected-transitions{
	    float: left;
    padding: 0px;
    border: 1px solid #ccc;
    width: 35%;
    min-height: 300px;
}
.revslide .selected-transitions ul{
	padding: 0;
    list-style: none;
}
.revslide .selected-transitions ul li{
	background: #f1f1f1;
    padding: 10px 15px;
    border-top: 1px dashed #ffffff;
}
.revslide #frames{
	list-style:none;
	    padding: 0;
}
.revslide #frames li{
	padding:10px;
}
.revslide #frames li:nth-child(even){
	background:#ffffff;
}
.revslide #frames li:nth-child(odd){
	background:#f1f1f1;
}
.revslide #frames li table{
	width:100%;
}
.revslide #frames li table td{
	padding:2px 10px;
}
.revslide #frames li table td input{
	padding:5px 10px;
	width:100px;
}
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content" style="background:#777;padding:0;">
				<ul class="buttons">
					<li><button id="setting" data-toggle="collapse" data-target="#settings"><span class="glyphicon glyphicon-cog"></span></button></li>
					<li><button><span class="glyphicon glyphicon-floppy-disk"></span></button></li>
					<li><button><span class="glyphicon glyphicon-trash"></span></button></li>
					<li><button><span class="glyphicon glyphicon-search"></span></button></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="row collapse" id="settings">
	<form id="slidersettings">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<ul  class="nav nav-pills">
					<li class="active"><a  href="#1c" data-toggle="tab">General</a></li>
					<li><a href="#2c" data-toggle="tab">Navigation</a></li>
				</ul>
				<div class="tab-content clearfix">
					<div class="tab-pane active" id="1c">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<h4>Dimension</h4>
									<input type="text" name="gridwidth" class="form-control form-control-short" placeholder="Width" value="{{ getSliderSetting('gridwidth', '') }}"> X <input type="text" name="gridheight" class="form-control form-control-short" placeholder="Height"  value="{{ getSliderSetting('gridheight', '') }}">
								</div>
								
								<div class="form-group">
									<h4>Slider Type</h4>
									<select name="sliderType" class="form-control">
										@foreach(getOptions('sliderType') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('sliderType', '')== $key ? 'selected':'' }}>{{ $option }}</option>
										@endforeach
									</select>
								</div>
								
								<div class="form-group">
									<h4>Slider Layout</h4>
									<select name="sliderLayout" class="form-control">
										@foreach(getOptions('sliderLayout') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('sliderLayout', '')== $key ? 'selected':'' }} >{{ $option }}</option>
										@endforeach
									</select>
								</div>
								
								<div class="form-group">
									<h4>Default Slide Duration</h4>
									<input type="text" name="delay" class="form-control" placeholder="" value="{{ getSliderSetting('delay', '') }}">
								</div>
								
								<div class="form-group">
									<h4>Shadow Type</h4>
									<select name="shadow" class="form-control">
										@foreach(getOptions('shadow') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('shadow', '')== $key ? 'selected':'' }}>{{ $option }}</option>
										@endforeach
									</select>
								</div>
								
								<div class="form-group">
									<h4>Spinner</h4>
									<select name="spinner" class="form-control">
										@foreach(getOptions('spinner') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('spinner', '')== $key ? 'selected':'' }}>{{ $option }}</option>
										@endforeach
									</select>
								</div>
							</div>
						
							<div class="col-sm-6">
								<div class="form-group">
									<h4>Stop Loop</h4>
									<label class="switch">
									  <input type="checkbox" name="stoploop" {{  getSliderSetting('stoploop', '')== 'off' ? '':'checked' }}>
									  <span class="slider"></span>
									</label>
								</div>
								
								<div class="form-group">
									<h4>Stop After Loops</h4>
									<input type="number" name="stopafterloop" class="form-control" placeholder="" value="{{ getSliderSetting('stopafterloop', '') }}">
								</div>
								
								<div class="form-group">
									<h4>Stop At Slide</h4>
									<input type="number" name="stopatslide" class="form-control" placeholder="" value="{{ getSliderSetting('stopatslide', '') }}">
								</div>
								
								<div class="form-group">
									<h4>Auto Height</h4>
									<label class="switch">
									  <input type="checkbox" name="autoheight" {{  getSliderSetting('autoheight', '')== 'off' ? '':'checked' }}>
									  <span class="slider"></span>
									</label>
								</div>
								
								<div class="form-group">
									<h4>Hide Thumbs On Mobile</h4>
									<label class="switch">
									  <input type="checkbox" name="hidethumbsonmobile" {{  getSliderSetting('hidethumbsonmobile', '')== 'off' ? '':'checked' }}>
									  <span class="slider"></span>
									</label>
								</div>
							</div>
						</div>
						
					</div>
					<div class="tab-pane" id="2c">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<h4>Enable Arrows</h4>
									<label class="switch">
									  <input type="checkbox" name="enablearrow" {{  getSliderSetting('enablearrow', '')== 'true' ? 'checked':'' }}>
									  <span class="slider"></span>
									</label>
								</div>
								<div class="form-group">
									<h4>Arrows Style</h4>
									<select name="arrow_style" class="form-control">
										@foreach(getOptions('arrow_style') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('arrow_style', '')== $key ? 'selected':'' }}>{{ $option }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<h4>Hide Arrows on Mobile</h4>
									<label class="switch">
									  <input type="checkbox" name="hideonmobile" {{  getSliderSetting('hideonmobile', '')== 'true' ? 'checked':'' }}>
									  <span class="slider"></span>
									</label>
								</div>
								<div class="form-group">
									<h4>Hide Arrows Under</h4>
									<input type="text" name="hideunder" class="form-control" placeholder="" value="{{ getSliderSetting('hideunder', '') }}">
								</div>
								<div class="form-group">
									<h4>Hide Arrows On Leave</h4>
									<label class="switch">
									  <input type="checkbox" name="hideonleave" {{  getSliderSetting('hideonleave', '')== 'true' ? 'checked':'' }}>
									  <span class="slider"></span>
									</label>
								</div>
							</div>
							<div class="col-sm-6">
								<h3>Left Arrow Position</h3>
								<div class="form-group">
									<h4>Horizontal Align</h4>
									<select name="l_halign" class="form-control">
										@foreach(getOptions('l_halign') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('l_halign', '')== $key ? 'selected':'' }}>{{ $option }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<h4>Vertical Align</h4>
									<select name="l_valign" class="form-control">
										@foreach(getOptions('l_valign') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('l_valign', '')== $key ? 'selected':'' }}>{{ $option }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<h4>Horizontal Offset</h4>
									<input type="text" name="l_horizoff" class="form-control" placeholder="20px" value="{{ getSliderSetting('l_horizoff', '') }}">
								</div>
								<div class="form-group">
									<h4>Vertical Offset</h4>
									<input type="text" name="l_vertoff" class="form-control" placeholder="0px" value="{{ getSliderSetting('l_vertoff', '') }}">
								</div>
								<h3 style="margin-top:30px;">Right Arrow Position</h3>
								<div class="form-group">
									<h4>Horizontal Align</h4>
									<select name="r_halign" class="form-control">
										@foreach(getOptions('r_halign') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('r_halign', '')== $key ? 'selected':'' }}>{{ $option }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<h4>Vertical Align</h4>
									<select name="r_valign" class="form-control">
										@foreach(getOptions('r_valign') as $key=>$option)
											<option value="{{ $key }}" {{  getSliderSetting('r_valign', '')== $key ? 'selected':'' }}>{{ $option }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<h4>Horizontal Offset</h4>
									<input type="text" name="r_horizoff" class="form-control" placeholder="20px" value="{{ getSliderSetting('r_horizoff', '') }}">
								</div>
								<div class="form-group">
									<h4>Vertical Offset</h4>
									<input type="text" name="r_vertoff" class="form-control" placeholder="0px" value="{{ getSliderSetting('r_vertoff', '') }}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="slides block">
					<ul class="slidethumbs">
						<li style="background-image:url('{{ url('uploads/thomas-jefferson-1.jpg') }}')"><div class="slidecount">#Slide1</div></li>
						<li style="background-image:url('{{ url('uploads/thomas-jefferson-1.jpg') }}')"><div class="slidecount">#Slide2</div></li>
						<li style="background-image:url('{{ url('uploads/thomas-jefferson-1.jpg') }}')"><div class="slidecount">#Slide3</div></li>
						<li style="background-image:url('{{ url('uploads/thomas-jefferson-1.jpg') }}')"><div class="slidecount">#Slide4</div></li>
						<li class="new">
							<a href="#"><span class="glyphicon glyphicon-plus"></span></a>
							<div class="slidecount">
								New Slide
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<form id="slidesettings">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="settings block">
				<div class="ibox-content" style="margin-bottom:10px;padding:0;">
						<ul  class="nav nav-pills">
							<li class="active"><a  href="#1a" data-toggle="tab">Main Background</a></li>
							<li><a href="#3a" data-toggle="tab">Slide Animation</a></li>
						</ul>
				</div>
				<div class="ibox-content">
						<div class="tab-content clearfix">
							<div class="tab-pane active" id="1a">
								<div class="form-group radio-enabled">
									<h4>Background Image</h4>
									<input type="radio" name="layer_bgtype" value="bgimage" checked>
									<input type="text" name="layer_bgimage" class="form-control" placeholder="" value="{{ getSliderSetting('bgimage', '') }}">
									<button class="addmedia">Add Image</button>
								</div>
								<div class="form-group radio-enabled">
									<h4>External URL</h4>
									<input type="radio" name="layer_bgtype" value="eximage">
									<input type="text" name="layer_eximage" class="form-control" placeholder="" value="">
								</div>
								<div class="form-group radio-enabled">
									<h4>Transparent</h4>
									<input type="radio" name="layer_bgtype" value="transparent">
									<input type="text" name="layer_transparent" class="form-control" placeholder="" value="">
								</div>
								<div class="form-group radio-enabled">
									<h4>Colored</h4>
									<input type="radio" name="layer_bgtype" value="colored">
									<input type="text" name="layer_colored" class="form-control" placeholder="" value="">
								</div>
							</div>
							<div class="tab-pane" id="3a">
								
								
								
								
								
								
								<div class="vertical-tab">
									@foreach(getOptions('slideanimation') as $key=>$val)
										<button class="tablinks" rel='{{ strtolower(str_replace(' ', '', $key)) }}'>{{ $key }}</button>
									@endforeach
								</div>
								@foreach(getOptions('slideanimation') as $key=>$val)
								<div id="{{ strtolower(str_replace(' ', '', $key)) }}" class="tabcontent">
									<ul>
									@foreach($val as $key=>$val)
										<li><input type="checkbox" name="transitions[]" value="{{ $key }}"> {{ $val }}</li>
									@endforeach
									</ul>
								</div>
								@endforeach
								<div class="selected-transitions">
									<ul>
										
									</ul>
									<input type="hidden" name="slide_transitions" value="">
								</div>
								<script>
								$(document).ready(function(){
									setTimeout(function(){ 
										var selected = $('input[name="slide_transitions"]').val().split(',');
										var html = '';
										for(var i=0;i<selected.length;i++){
											html = html+"<li>"+selected[i]+"</li>";
										}
										$('.selected-transitions ul').html(html);
										
									}, 200);
									$('.tablinks').click(function(){
										var i, tabcontent, tablinks;
										$('.tablinks').removeClass('active');
										tabcontent = document.getElementsByClassName("tabcontent");
										for (i = 0; i < tabcontent.length; i++) {
											tabcontent[i].style.display = "none";
										}
										$(this).addClass('active');
										$('#'+$(this).attr('rel')).show();
									})
									$('input[name="transitions[]"]').click(function(){
										$('input[name="slide_transitions"]').val($('input[name="transitions[]"]:checked').map(function() { return this.value;}).get().join(','));
										setTimeout(function(){ 
											var selected = $('input[name="slide_transitions"]').val().split(',');
											var html = '';
											for(var i=0;i<selected.length;i++){
												html = html+"<li>"+selected[i]+"</li>";
											}
											$('.selected-transitions ul').html(html);
										}, 200);
									})
								})
								</script>
								
								
								
								
								
								
								
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="styles block" id="styles">
				<div class="ibox-content" style="margin-bottom:10px;padding:0;">
					<ul  class="nav nav-pills">
						<li class="active"><a  href="#1b" data-toggle="tab">Style</a></li>
						<li><a href="#2b" data-toggle="tab">Attributes</a></li>
						<li><a href="#3b" data-toggle="tab">Slide Animation</a></li>
						<li><a href="#4b" data-toggle="tab">Action</a></li>
					</ul>
				</div>
				<div class="ibox-content">
						<div class="tab-content clearfix">
							<div class="tab-pane active" id="1b">
								<div class="inline-field">
									<label>Font Size</label><input type="text" class="form-control" name="layer_fontsize">
								</div>
								<div class="inline-field">
									<label>Line Height</label><input type="text" class="form-control" name="layer_lineheight">
								</div>
								<div class="inline-field">
									<label>Color</label><input type="text" class="form-control" name="layer_color" style="width:100px">
								</div>
								<div class="inline-field">
									<label>Font Weight</label>
									<select style="width:61px" class="form-control" name="layer_fontweight">
										@foreach(getOptions('fontweight') as $key=>$option)
											<option value="{{ $key }}">{{ $option }}</option>
										@endforeach
									</select>
								</div>
								<div class="inline-field">
									<label>Font Family</label><input type="text" class="form-control" name="layer_fontfamily" style="width:100px">
								</div>
								<div class="inline-field">
									<label>Background Color</label><input type="text" class="form-control" name="layer_bgcolor">
								</div>
								<div class="inline-field">
									<label>Padding</label><input type="text" class="short form-control" name="layer_pleft" placeholder="Left"><input class="short form-control" type="text" name="layer_pright" placeholder="Right"><input class="short form-control" type="text" name="layer_ptop" placeholder="Top"><input class="short form-control" type="text" name="layer_pbottom" placeholder="Bottom">
								</div>
								<hr />
								<div class="inline-field">
									<label>Horizontal Align</label>
									<select name="layer_halign" class="form-control">
										@foreach(getOptions('halign') as $key=>$option)
											<option value="{{ $key }}">{{ $option }}</option>
										@endforeach
									</select>
								</div>
								<div class="inline-field">
									<label>Horizontal Offset</label><input type="text" name="layer_hoffset" class="form-control">
								</div>
								<div class="inline-field">
									<label>Vertical Align</label>
									<select name="layer_valign" class="form-control">
										@foreach(getOptions('valign') as $key=>$option)
											<option value="{{ $key }}">{{ $option }}</option>
										@endforeach
									</select>
								</div>
								<div class="inline-field">
									<label>Vertical Offset</label><input type="text" name="layer_voffset" class="form-control">
								</div>
							</div>
							<div class="tab-pane" id="2b">
								<div class="inline-field">
									<label>Rel</label><input type="text" name="layer_rel" class="form-control">
								</div>
								<div class="inline-field">
									<label>Title</label><input type="text" name="layer_title" class="form-control">
								</div>
								<div class="inline-field">
									<label>Class</label><input type="text" name="layer_class" class="form-control">
								</div>
							</div>
							<div class="tab-pane" id="3b">
								<div class="inline-field">
									<label>Animation</label>
									<select name="layer_animation" class="form-control" style="width: 220px;">
										@foreach(getOptions('layeranimation') as $key=>$option)
											<option value="{{ $key }}">{{ $option['label'] }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="tab-pane" id="4b">
								<div class="inline-field">
									<label>Target</label>
									<select name="layer_target" class="form-control" style="width: 100px;">
										<option value="_self">Self</option>
										<option value="_blank">New Tab</option>
									</select>
								</div>
								<div class="inline-field">
									<label>Link</label>
									<input type="text" name="layer_link" class="form-control">
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content" style="padding:0;">
				<div class="editor block">
					<div class="layer_controls">
						<div class="col-sm-12 text-center">
							<ul>
								<li>
									<a href="#" id="addlayer" class="btn">Add Layer</a>
									<ul>
										<li><a href="#" id="addnewlayer_html">Text/HTML</a></li>
										<li><a href="#" id="addnewlayer_btn">Button</a></li>
									</ul>
								</li>
								<li>
									<button id="editlayer" class="btn"><i class="glyphicon glyphicon-pencil"></i></button>
								</li>
								<li>
									<a href="#" id="deletelayer" class="btn"><i class="glyphicon glyphicon-trash"></i></a>
								</li>
								<li>
									<a href="#" id="duplicatelayer" class="btn"><i class="glyphicon glyphicon-duplicate"></i></a>
								</li>
								<li>
									<button id="savelayer" class="btn"><i class="glyphicon glyphicon-floppy-disk"></i></button>
								</li>
							</ul>
							<textarea id="textlayer" class="textlayer"></textarea>
						</div>
					</div>
					<div id="editor">
					
					
					
					
					
					
					<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="layer-animations" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
						<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
							<ul>	
							<!-- SLIDES  -->
								<li  data-transition="parallaxhorizontal" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="../../assets/images/datcolor-70x70.jpg"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Letters Fly In" data-description="">
									<!-- MAIN IMAGE -->
									<img src="{{ url('uploads/thomas-jefferson-1.jpg') }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
									<!-- LAYERS -->

									<!-- LAYER NR. 1 -->
									<div class="tp-caption NotGeneric-Title tp-resizeme" 
										 id="slide-1-layer-1" 
										 data-x="center" data-hoffset="0" 
										 data-y="center" data-voffset="-150" 
										data-width="['auto','auto','auto','auto']"
										data-height="['auto','auto','auto','auto']"
										data-start="1000" 
										data-responsive_offset="on" 
										data-frames='[{"delay":0,"speed":300,"frame":"0","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
										data-elementdelay="0.05" 
										style="z-index: 5; white-space: nowrap; font-size: 30px; line-height: 30px;">LETTERS FROM BOTTOM 
										
									</div>
									<!-- data-actions='[{"event": "click", "action": "simplelink", "target": "_self", "url": "http://www.themepunch.com"}]' -->
								</li>
							</ul>
						</div>
					</div>
					
					
					
					
					
					
					
					
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-12">
						<div class="ibox float-e-margins">
							<div class="ibox-content" style="padding:0;">
								<div class="frames">
									<form id="layersettings">
									<ul id="frames">
										<li>
											<table>
											<tr>
												<td width="100">Type</td>
												<td width="400">Layer</td>
												<td>Starts at</td>
											</tr>
											</table>
										</li>
										<li data-rel='slide-1-layer-1'>
											<table>
											<tr>
												<td width="100">html</td>
												<td width="400">LETTERS FROM BOTTOM</td>
												<td><input type="text" name="hidden_layer_start[]" value="1000"></td>
											</tr>
											</table>
											<input type="hidden" name="hidden_layer_id[]" value="slide-1-layer-1">
											<input type="hidden" name="hidden_layer_type[]" value="html">
											<input type="hidden" name="hidden_layer_rel[]" value="">
											<input type="hidden" name="hidden_layer_title[]" value="">
											<input type="hidden" name="hidden_layer_class[]" value="">
											<input type="hidden" name="hidden_layer_halign[]" value="center">
											<input type="hidden" name="hidden_layer_hoffset[]" value="100">
											<input type="hidden" name="hidden_layer_valign[]" value="center">
											<input type="hidden" name="hidden_layer_voffset[]" value="-150">
											<input type="hidden" name="hidden_layer_fontsize[]" value="30px">
											<input type="hidden" name="hidden_layer_lineheight[]" value="30px">
											<input type="hidden" name="hidden_layer_color[]" value="rgba(255,255,255,1.00)">
											<input type="hidden" name="hidden_layer_fontweight[]" value="800">
											<input type="hidden" name="hidden_layer_fontfamily[]" value="Raleway">
											<input type="hidden" name="hidden_layer_bgcolor[]" value="transparent">
											<input type="hidden" name="hidden_layer_pleft[]" value="0">
											<input type="hidden" name="hidden_layer_pright[]" value="0">
											<input type="hidden" name="hidden_layer_ptop[]" value="10px">
											<input type="hidden" name="hidden_layer_pbottom[]" value="10px">
											<input type="hidden" name="hidden_layer_text[]" value="LETTERS FROM BOTTOM">
											<input type="hidden" name="hidden_layer_animation[]" value="">											
											<input type="hidden" name="hidden_layer_target[]" value="">
											<input type="hidden" name="hidden_layer_link[]" value="">
										</li>
									</ul>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer')
<script type="text/javascript">
	var tpj=jQuery;					
	var revapi116;
	var revapi = tpj(document).ready(function() {
		if(tpj("#rev_slider_1_1").revolution == undefined){
			revslider_showDoubleJqueryError("#rev_slider_1_1");
		}else{
			revapi116 = tpj("#rev_slider_1_1").show().revolution({
				sliderType:"{{ getSliderSetting('sliderType', '') }}",
				jsFileLocation:"{{ asset('revolution/js/') }}",
				sliderLayout:"{{ getSliderSetting('sliderLayout', '') }}",
				dottedOverlay:"none",
				delay:{{ getSliderSetting('delay', '') }},
				navigation: {
					keyboardNavigation:"off",
					keyboard_direction: "horizontal",
					mouseScrollNavigation:"off",
					onHoverStop:"off",
					touch:{
						touchenabled:"on",
						swipe_threshold: 75,
						swipe_min_touches: 1,
						swipe_direction: "horizontal",
						drag_block_vertical: false
					}
					,
					arrows: {
						style:"{{ getSliderSetting('arrow_style', '') }}",
						enable:false,
						hide_onmobile:{{ getSliderSetting('hideonmobile', '') }},
						hide_under:{{ getSliderSetting('hideunder', '') }},
						hide_onleave:{{ getSliderSetting('hideonleave', '') }},
						hide_delay:200,
						hide_delay_mobile:1200,
						tmp:'',
						left: {
							h_align:"{{ getSliderSetting('l_halign', '') }}",
							v_align:"{{ getSliderSetting('l_valign', '') }}",
							h_offset:{{ getSliderSetting('l_horizoff', '') }},
							v_offset:{{ getSliderSetting('l_vertoff', '') }}
						},
						right: {
							h_align:"{{ getSliderSetting('r_halign', '') }}",
							v_align:"{{ getSliderSetting('r_valign', '') }}",
							h_offset:{{ getSliderSetting('r_horizoff', '') }},
							v_offset:{{ getSliderSetting('r_vertoff', '') }}
						}
					}
				},
				viewPort: {
					enable:true,
					outof:"pause",
					visible_area:"80%"
				},
				gridwidth:{{ getSliderSetting('gridwidth', '') }},
				gridheight:{{ getSliderSetting('gridheight', '') }},
				lazyType:"none",
				shadow:{{ getSliderSetting('shadow', '') }},
				spinner:"{{ getSliderSetting('spinner', '') }}",
				stopLoop:"{{ getSliderSetting('stoploop', '') }}",
				stopAfterLoops:{{ getSliderSetting('stopafterloop', '') }},
				stopAtSlide:{{ getSliderSetting('stopatslide', '') }},
				shuffle:"off",
				autoHeight:"{{ getSliderSetting('autoheight', '') }}",
				hideThumbsOnMobile:"{{ getSliderSetting('hidethumbsonmobile', '') }}",
				hideSliderAtLimit:0,
				hideCaptionAtLimit:0,
				hideAllCaptionAtLilmit:0,
				debugMode:false,
				fallbacks: {
					simplifyAll:"off",
					nextSlideOnWindowFocus:"off",
					disableFocusListener:false,
				}
			});
		}
	});	/*ready*/
	$(document).ready(function(){
			var slide = 1; // ID OF SLIDER
			var prehtmllayer_container = '<div class="tp-mask-wrap"><div class="tp-caption NotGeneric-Title tp-resizeme" id="slide-'+slide+'-layer-LAYERNUMBER"'+
				'style="z-index: 5;visibility:inherit; white-space: nowrap; font-size: 30px; line-height: 30px;">Caption Text</div></div>';
				
			var prebuttonlayer_container = '<div class="tp-mask-wrap"><div class="tp-caption tp-resizeme" id="slide-'+slide+'-layer-LAYERNUMBER"'+
				'style="z-index: 9;visibility:inherit;white-space: nowrap; font-weight: 800;padding:10px 20px;text-align:center;background-color:rgba(41, 46, 49, 1.00);border-color:rgba(255, 255, 255, 0);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Button</div></div>';
				
			var prehtmllayer_elements = '<li data-rel="slide-'+slide+'-layer-LAYERNUMBER"> '+
				'<table><tr><td width="100">html</td>'+
				'<td width="400">Caption Text</td>'+
				'<td>TIMELINE</td></tr></table>'+
				'<input type="hidden" name="hidden_layer_id[]" value="slide-'+slide+'-layer-LAYERNUMBER">' +
				'<input type="hidden" name="hidden_layer_type[]" value="html">' +
				'<input type="hidden" name="hidden_layer_rel[]" value="">'+
				'<input type="hidden" name="hidden_layer_title[]" value="">'+
				'<input type="hidden" name="hidden_layer_class[]" value="">'+
				'<input type="hidden" name="hidden_layer_halign[]" value="center">'+
				'<input type="hidden" name="hidden_layer_hoffset[]" value="100">'+
				'<input type="hidden" name="hidden_layer_valign[]" value="center">'+
				'<input type="hidden" name="hidden_layer_voffset[]" value="-150">'+
				'<input type="hidden" name="hidden_layer_fontsize[]" value="30px">'+
				'<input type="hidden" name="hidden_layer_lineheight[]" value="30px">'+
				'<input type="hidden" name="hidden_layer_color[]" value="rgba(255,255,255,1.00)">'+
				'<input type="hidden" name="hidden_layer_fontweight[]" value="800">'+
				'<input type="hidden" name="hidden_layer_fontfamily[]" value="Raleway">'+
				'<input type="hidden" name="hidden_layer_bgcolor[]" value="transparent">'+
				'<input type="hidden" name="hidden_layer_pleft[]" value="0">'+
				'<input type="hidden" name="hidden_layer_pright[]" value="0">'+
				'<input type="hidden" name="hidden_layer_ptop[]" value="10px">'+
				'<input type="hidden" name="hidden_layer_pbottom[]" value="10px">'+
				'<input type="hidden" name="hidden_layer_text[]" value="Caption Text">'+
				'<input type="hidden" name="hidden_layer_animation[]" value="">'+
				'<input type="hidden" name="hidden_layer_target[]" value="">'+
				'<input type="hidden" name="hidden_layer_link[]" value="">'+
			'</li>';
			
			var prebtnlayer_elements = '<li data-rel="slide-'+slide+'-layer-LAYERNUMBER"> '+
				'<table><tr><td width="100">btn</td>'+
				'<td width="400">Button</td>'+
				'<td>TIMELINE</td></tr></table>'+
				'<input type="hidden" name="hidden_layer_id[]" value="slide-'+slide+'-layer-LAYERNUMBER">' +
				'<input type="hidden" name="hidden_layer_type[]" value="btn">' +
				'<input type="hidden" name="hidden_layer_rel[]" value="">'+
				'<input type="hidden" name="hidden_layer_title[]" value="">'+
				'<input type="hidden" name="hidden_layer_class[]" value="">'+
				'<input type="hidden" name="hidden_layer_halign[]" value="center">'+
				'<input type="hidden" name="hidden_layer_hoffset[]" value="100">'+
				'<input type="hidden" name="hidden_layer_valign[]" value="center">'+
				'<input type="hidden" name="hidden_layer_voffset[]" value="-150">'+
				'<input type="hidden" name="hidden_layer_fontsize[]" value="30px">'+
				'<input type="hidden" name="hidden_layer_lineheight[]" value="30px">'+
				'<input type="hidden" name="hidden_layer_color[]" value="rgba(255,255,255,1.00)">'+
				'<input type="hidden" name="hidden_layer_fontweight[]" value="800">'+
				'<input type="hidden" name="hidden_layer_fontfamily[]" value="Raleway">'+
				'<input type="hidden" name="hidden_layer_bgcolor[]" value="rgba(41, 46, 49, 1.00)">'+
				'<input type="hidden" name="hidden_layer_pleft[]" value="20px">'+
				'<input type="hidden" name="hidden_layer_pright[]" value="20px">'+
				'<input type="hidden" name="hidden_layer_ptop[]" value="10px">'+
				'<input type="hidden" name="hidden_layer_pbottom[]" value="10px">'+
				'<input type="hidden" name="hidden_layer_text[]" value="Button">'+
				'<input type="hidden" name="hidden_layer_animation[]" value="">'+
				'<input type="hidden" name="hidden_layer_target[]" value="_self">'+
				'<input type="hidden" name="hidden_layer_link[]" value="http://">'+
			'</li>';
			
			$('#addnewlayer_html').click(function(){
				$('.tp-loop-wrap').append(prehtmllayer_container.replace('LAYERNUMBER',$('#frames li').length));
				var lastframe= parseFloat($('#frames li').eq($('#frames li').length-1).find('input').val())+300;
				if($('#frames li').length==1){
					lastframe = 1000;
				}
				$('#frames').append(prehtmllayer_elements.replace('TIMELINE', '<input type="text" name="hidden_layer_start[]" value="'+lastframe+'">').replace('LAYERNUMBER',$('#frames li').length));
				return false;
			})
			$('#addnewlayer_btn').click(function(){
				$('.tp-loop-wrap').append(prebuttonlayer_container.replace('LAYERNUMBER',$('#frames li').length));
				var lastframe= parseFloat($('#frames li').eq($('#frames li').length-1).find('input').val())+300;
				if($('#frames li').length==1){
					lastframe = 1000;
				}
				$('#frames').append(prebtnlayer_elements.replace('TIMELINE', '<input type="text" name="hidden_layer_start[]" value="'+lastframe+'">').replace('LAYERNUMBER',$('#frames li').length));
				return false;
			})
			$('#editlayer').click(function(e){
				$('#textlayer').val($('#frames li.selected input[name="hidden_layer_text[]"]').val());
				$('#textlayer').show();
				$(this).prop("disabled", true);
				$('#savelayer').prop("disabled", false);
				e.stopPropagation();
				return false;
			})
			$('#savelayer').click(function(e){
				$('#frames li.selected input[name="hidden_layer_text[]"]').val($('#textlayer').val());
				$( ".tp-mask-wrap.selected .tp-caption").html($('#textlayer').val());
				$('#textlayer').hide();
				$(this).prop("disabled", true);
				$('#editlayer').prop("disabled", false);
				return false;
			})
			$('#deletelayer').click(function(e){
				$('#frames li.selected').remove();
				$( ".tp-mask-wrap.selected").remove();
				return false;
			})
			$('#duplicatelayer').click(function(e){
				$('#frames li.selected').clone().removeClass('selected').attr('data-rel', 'slide-'+slide+'-layer-'+$('#frames li').length).appendTo($('#frames'));
				var layer = parseFloat($('#frames li').length)-1;
				$( ".tp-mask-wrap.selected").clone().find('.tp-caption').attr('id', 'slide-'+slide+'-layer-'+layer).parents('.tp-mask-wrap.selected').removeClass('selected').appendTo($('.tp-loop-wrap'));
				return false;
			})
	})
</script>
@endsection
