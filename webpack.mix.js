let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js');

mix.js('resources/assets/js/jqueryui-ruler/js/jquery.ui.ruler.js', 'public/revolution/js');
mix.js('resources/assets/js/jquery-ui.min.js', 'public/revolution/js');

mix.copy(['resources/assets/js/jqueryui-ruler/css/jquery.ui.ruler.css'], 'public/revolution/css');
mix.copyDirectory('resources/assets/js/revolution', 'public/revolution');



mix.sass('resources/assets/sass/app.scss', 'public/css');
