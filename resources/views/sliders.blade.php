@extends('layouts.app')

@section('extra')
<style>
.revslide .slidethumbs{
	list-style: none;
    margin: 0;
    padding: 0;
}
.revslide .slidethumbs li{
	display: inline-block;
    width: 280px;
	height:230px;
	background-size:cover;
	background-position:center;
	margin:10px;
	position:relative;
}
.revslide .slidethumbs li .slidecount{
	background: #000000;
    color: #ffffff;
    text-align: left;
    padding: 5px 10px;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
}
.revslide .slidethumbs li .slidecount .controls{
	position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
}
.revslide .slidethumbs li .slidecount .controls a{
	font-size: 15px;
    width: 34px;
    text-align: center;
    padding-top: 5px;
    color: #ffffff;
    padding-bottom: 3px;
    background: #353535;
    border-left: 1px solid #000000;
	float: left;
}
.revslide .slidethumbs li .slidecount .controls a:hover{
background:#319cf4;
}
.revslide .slidethumbs li img{
	max-width:100%;
}
.revslide .slidethumbs li > a{
	position:absolute;
	left:0;
	right:0;
	top:0;
	bottom:0;
}
.revslide .slidethumbs li.new{
	border-left: 1px dashed #cccccc;
    border-top: 1px dashed #cccccc;
    border-right: 1px dashed;
}
.revslide .slidethumbs li.new a{
	    font-size: 45px;
    color: #000000;
    text-align: center;
    line-height: 5;
}
</style>
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="slides block">
					<ul class="slidethumbs">
						@foreach($sliders as $slider)
						<li style="background-image:url('{{ url('uploads/thomas-jefferson-1.jpg') }}')">
							<a href="{{ route('sliders.show', $slider->id) }}"></a>
							<div class="slidecount">
								#{{ $slider->slidertitle }}
								<div class="controls">
								<a href="{{ route('sliders.show', $slider->id) }}#setting"><span class="glyphicon glyphicon-cog"></span></a>
								<a href="{{ route('sliders.show', $slider->id) }}"><span class="glyphicon glyphicon-pencil"></span></a>
								</div>
							</div>
						</li>
						@endforeach
						<li class="new">
							<a href="#"><span class="glyphicon glyphicon-plus"></span></a>
							<div class="slidecount">
								New Slider
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer')

@endsection
